RELEASE=elasticsearch-v2
REPLICAS=1
MIN_REPLICAS=1
STORAGE_CLASS=local-storage

helm install ${RELEASE} stable/elasticsearch \
      --set client.replicas=${MIN_REPLICAS} \
      --set master.replicas=${REPLICAS} \
      --set master.persistence.storageClass=${STORAGE_CLASS} \
      --set data.replicas=${MIN_REPLICAS} \
      --set data.persistence.storageClass=${STORAGE_CLASS} \
      --set master.podDisruptionBudget.minAvailable=${MIN_REPLICAS} \
      --set cluster.env.MINIMUM_MASTER_NODES=${MIN_REPLICAS} \
      --set cluster.env.RECOVER_AFTER_MASTER_NODES=${MIN_REPLICAS} \
      --set cluster.env.EXPECTED_MASTER_NODES=${MIN_REPLICAS} \
