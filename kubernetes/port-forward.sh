#!/bin/bash
R=`kubectl get pods | awk 'NR ==2 {print $3}'`
Rp="Running"
if [ "$R" = "$Rp" ];
then
   kill -9 `ps -ef | grep 3306 | awk '{print $2}'`
   kubectl port-forward --address 0.0.0.0 service/backend 3306:3306 & >> portBE_logs.txt ; sleep 5 ; echo -ne "\n"
   echo
else
   for i in {1..5}
   do
     sleep 5
     R=`kubectl get pods | awk 'NR==2 {print $3}'`
     echo "Trying to Port forward for ${i} time(s)"
     if [ "$R" = "$Rp" ]; 
     then
         kill -9 `ps -ef | grep 3306 | awk '{print $2}'`
         kubectl port-forward --address 0.0.0.0 service/backend 3306:3306 & >> portBE_logs.txt ; sleep 5 ; echo -ne "\n"
         echo
         break
     fi
     if [ $i = 5 ];
     then
        echo "Pod is not online, failed to port-forward the service/backend 3306:3306"
     fi
   done
fi
R=`kubectl get pods | awk 'NR ==3 {print $3}'`
Rp="Running"
if [ "$R" = "$Rp" ];
then
     kill -9 `ps -ef | grep 8070 | awk '{print $2}'`
     kubectl port-forward --address 0.0.0.0 service/devops-practice 8070:8070 & >> portFE_logs.txt ; sleep 5 ; echo -ne "\n"
     echo
else
   for i in {1..5}
   do
     sleep 30
     R=`kubectl get pods | awk 'NR==3 {print $3}'`
     echo "Trying to Port forward App Pod for ${i} time(s)"
     if [ "$R" = "$Rp" ];
     then
         kill -9 `ps -ef | grep 8070 | awk '{print $2}'`
         kubectl port-forward --address 0.0.0.0 service/devops-practice 8070:8070 & >> portFE_logs.txt ; sleep 5 ; echo -ne "\n"
         exit 0
         break
     fi
     if [ $i = 5 ];
     then
     echo "Pod is not online, failed to port-forward the service/devops-practice 8070:8070"
     exit 1
     fi
   done
fi

